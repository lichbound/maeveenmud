﻿using Lichbound.Nightshadow.Models;
using Lichbound.Nightshadow.Scriptables;
using Lichbound.WraithLib.Ioc;
using Lichbound.WraithLib.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Commands
{
    [Event("Start")]
    public class StartStuff : Command
    {
        [Inject]
        private MapModel _map;
        [Inject]
        private PlayerModel _player;

        public override void Execute(object[] args = null)
        {
            _map.LoadMap("testmap");

            _player.worldPos = _map.GetTiles()[0].pos;

            _dispatcher.Dispatch("set_player");

            _dispatcher.Dispatch("draw_map");
        }
    }
}
