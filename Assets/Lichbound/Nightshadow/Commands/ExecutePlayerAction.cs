﻿using Lichbound.Nightshadow.Models;
using Lichbound.Nightshadow.Scriptables;
using Lichbound.WraithLib.Extensions;
using Lichbound.WraithLib.Ioc;
using Lichbound.WraithLib.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Commands
{
    [Event("player_act")]
    public class ExecutePlayerAction : Command
    {
        [Inject]
        private PlayerModel _player;
        [Inject]
        private MapModel _map;

        public override void Execute(object[] args = null)
        {
            PlayerAction action = (PlayerAction)args[0];

            switch (action)
            {
                case PlayerAction.MoveForward:
                    Move(_player.worldPos.ToVector2Int() + _player.worldDir.ToVector2Int());
                    break;
                case PlayerAction.MoveBack:
                    Move(_player.worldPos.ToVector2Int() - _player.worldDir.ToVector2Int());
                    break;
                case PlayerAction.MoveLeft:
                    Move(_player.worldPos.ToVector2Int() + _player.worldDir.Rotate(90).ToVector2Int());
                    break;
                case PlayerAction.MoveRight:
                    Move(_player.worldPos.ToVector2Int() - _player.worldDir.Rotate(90).ToVector2Int());
                    break;
                case PlayerAction.TurnLeft:
                    Turn(90);
                    break;
                case PlayerAction.TurnRight:
                    Turn(-90);
                    break;
                case PlayerAction.InteractProp:
                    InteractProp((Map.Prop)args[1]);
                    break;
                default:
                    Debug.LogError("Invalid player action");
                    break;
            }
        }

        private void InteractProp(Map.Prop prop)
        {
            if (prop.WithinRange(_player.worldPos.ToVector2Int()))
                _dispatcher.Dispatch("playerview_interactprop", new object[] { prop });
        }

        private void Move(Vector2Int newPos)
        {
            if (_map.GetTile(newPos.x, newPos.y) != null)
            {
                if (!_map.IsPathBlocked((int)_player.worldPos.x, (int)_player.worldPos.y, newPos.x, newPos.y))
                {
                    _player.worldPos = newPos;
                    _dispatcher.Dispatch("playerview_move");
                    return;
                }
            }

            _dispatcher.Dispatch("playerview_enable");
        }

        private void Turn(float degrees)
        {
            _player.worldDir = _player.worldDir.Rotate(degrees);
            _dispatcher.Dispatch("playerview_turn");
        }
    }
}
