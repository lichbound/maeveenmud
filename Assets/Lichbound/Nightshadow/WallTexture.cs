﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow
{
    public class WallTexture
    {
        private Texture2D _texture;
        private Dictionary<string, RectInt> _walls = new Dictionary<string, RectInt>();

        public WallTexture(Texture2D texture, TextAsset txtAsset)
        {
            _texture = texture;
            ParseJson(txtAsset.text);
        }

        public WallTexture(string name, string tex)
        {
            _texture = Resources.Load<Texture2D>(tex);

            string json = Resources.Load<TextAsset>(name).text;
            ParseJson(json);
        }

        private void ParseJson(string json)
        {
            JsonData obj = JsonMapper.ToObject(json);
            if (obj.ContainsKey("frames"))
            {
                foreach (var key in obj["frames"].Keys)
                {
                    int x = (int)obj["frames"][key]["frame"]["x"];
                    int y = (int)obj["frames"][key]["frame"]["y"];
                    int w = (int)obj["frames"][key]["frame"]["w"];
                    int h = (int)obj["frames"][key]["frame"]["h"];
                    _walls.Add(key, new RectInt(x, y, w, h));
                }
            }
        }

        public Vector2[] GetUV(string key)
        {
            RectInt src;
            if (_walls.ContainsKey(key))
                src = _walls[key];
            else
                src = _walls.First().Value;

            Vector2[] uvs = new Vector2[4];
            uvs[0] = GetTexelCoord(src.x,             src.y + src.height, _texture.width, _texture.height);
            uvs[1] = GetTexelCoord(src.x + src.width, src.y + src.height, _texture.width, _texture.height);
            uvs[2] = GetTexelCoord(src.x,             src.y,              _texture.width, _texture.height);
            uvs[3] = GetTexelCoord(src.x + src.width, src.y,              _texture.width, _texture.height);
            return uvs;
        }

        public Dictionary<string, Texture2D> GetWallTextures()
        {
            Dictionary<string, Texture2D> wallTextures = new Dictionary<string, Texture2D>();
            foreach (var e in _walls)
            {
                Color[] texRect = _texture.GetPixels(e.Value.x, _texture.height - e.Value.y - e.Value.height, e.Value.width, e.Value.height);
                Texture2D wallTex = new Texture2D(e.Value.width, e.Value.height);
                wallTex.SetPixels(texRect);
                wallTex.Apply();
                wallTextures.Add(e.Key, wallTex);
            }
            return wallTextures;
        }

        public string[] GetWallNames()
        {
            return _walls.Keys.ToArray();
        }

        public Texture2D GetTexture()
        {
            return _texture;
        }

        private Vector2 GetTexelCoord(float x, float y, float w, float h)
        {
            return new Vector2(x / w, 1f - (y / h));
        }
    }
}
