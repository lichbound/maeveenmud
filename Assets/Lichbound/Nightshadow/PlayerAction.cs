﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichbound.Nightshadow
{
    public enum PlayerAction
    {
        MoveForward,
        MoveBack,
        MoveLeft,
        MoveRight,
        TurnLeft,
        TurnRight,
        InteractProp
    }
}
