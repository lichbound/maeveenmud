﻿using Lichbound.Nightshadow;
using Lichbound.Nightshadow.Behaviours;
using Lichbound.Nightshadow.Models;
using Lichbound.Nightshadow.Scriptables;
using Lichbound.WraithLib.Ioc;
using Lichbound.WraithLib.Mvc;
using Lichbound.WraithLib.Signals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace Lichbound.Nightshadow.Views
{
    public class MapMediator : Mediator<MapView>
    {
        [Inject]
        private MapModel _map;

        protected override void Destroy()
        {
        }

        protected override void Initialize()
        {
            _dispatcher.Listen("draw_map", OnDrawMap);
            View.onPropClicked.AddListener(OnPropClicked);

            WallTexture wt = new WallTexture("Textures/walls", "Textures/walls");
            View.SetTexture(wt);
        }

        private void OnDrawMap(object[] args)
        {
            View.CreateContainer();
            _map.ForEach(View.DrawTile, View.DrawProp);
            View.CombineTiles();
            View.DeleteContainer();
        }

        private void OnPropClicked(Map.Prop prop)
        {
            _dispatcher.Dispatch("player_act", new object[] { PlayerAction.InteractProp, prop });
        }
    }

    public class MapView : View
    {
        public ISignal<Map.Prop> onPropClicked = new Signal<Map.Prop>();

        private GameObject _container;
        private GameObject _props;
        private WallTexture _texture;

        public void SetTexture(WallTexture tex)
        {
            _texture = tex;
        }

        public void CreateContainer()
        {
            _container = new GameObject("tiles");
            _props = new GameObject("props");
            _props.transform.SetParent(transform);
        }

        public void DrawProp(Map.Prop prop)
        {
            if (prop != null)
            {
                GameObject propGo = (GameObject)PrefabUtility.InstantiatePrefab(prop.prefab);
                propGo.transform.position = prop.pos;
                propGo.transform.SetParent(_props.transform);
                PropController ctrl = propGo.GetComponent<PropController>();
                ctrl.onClicked = onPropClicked;
                ctrl.prop = prop;
            }
        }

        public void DrawTile(Map.Tile tile)
        {
            if (tile != null)
            {
                var tileObj = Resources.Load("Tile");
                GameObject tileGo = (GameObject)PrefabUtility.InstantiatePrefab(tileObj);
                //PrefabUtility.UnpackPrefabInstance(tileGo, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                tileGo.transform.position = new Vector3(tile.pos.x, 0, tile.pos.y);
                tileGo.transform.SetParent(_container.transform);
                TileController ctrl = tileGo.GetComponent<TileController>();
                ctrl.tile = tile;
                ctrl.UpdateActiveWalls(tile.activeWalls);
            }
        }

        public void CombineTiles()
        {
            GameObject combined = new GameObject("combined map");
            combined.isStatic = true;
            combined.transform.SetParent(this.transform);
            List<CombineInstance> combinedInstances = new List<CombineInstance>();

            foreach (Transform tile in _container.transform)
            {
                TileController controller = tile.gameObject.GetComponent<TileController>();
                foreach (Transform wall in tile.transform)
                {
                    if (wall.gameObject.activeInHierarchy)
                    {
                        Map.WallType wallType = (Map.WallType)Enum.Parse(typeof(Map.WallType), wall.gameObject.name);
                        string wallTexture = controller.tile.wallTextures[(int)wallType];
                        MeshFilter filter = wall.gameObject.GetComponentInChildren<MeshFilter>();
                        CombineInstance instance = new CombineInstance();
                        instance.mesh = Instantiate(filter.sharedMesh);
                        instance.transform = filter.transform.localToWorldMatrix;
                        instance.mesh.SetUVs(0, _texture.GetUV(wallTexture));
                        combinedInstances.Add(instance);
                    }
                }
            }

            MeshFilter combinedFilter = combined.AddComponent<MeshFilter>();
            combinedFilter.mesh = new Mesh();
            combinedFilter.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            combinedFilter.mesh.CombineMeshes(combinedInstances.ToArray());

            MeshRenderer combinedRenderer = combined.AddComponent<MeshRenderer>();
            combinedRenderer.material = Resources.Load<Material>("Materials/New Material");
            combinedRenderer.material.SetTexture("_MainTex", _texture.GetTexture());
            combinedRenderer.receiveShadows = false;
            combinedRenderer.shadowCastingMode = ShadowCastingMode.Off;
        }

        public void DeleteContainer()
        {
            Destroy(_container);
        }
    }
}
