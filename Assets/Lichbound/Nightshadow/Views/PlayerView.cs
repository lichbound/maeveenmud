﻿using Lichbound.WraithLib.Ioc;
using Lichbound.WraithLib.Mvc;
using Lichbound.WraithLib.Signals;
using Lichbound.WraithLib.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Lichbound.WraithLib.Extensions;
using Lichbound.Nightshadow.Models;
using Lichbound.Nightshadow.Scriptables;

namespace Lichbound.Nightshadow.Views
{
    public class PlayerMediator : Mediator<PlayerView>
    {
        [Inject]
        private PlayerModel _player;

        protected override void Destroy()
        {

        }

        protected override void Initialize()
        {
            View.action.AddListener(OnPlayerAction);

            _dispatcher.Listen("set_player", OnSetPlayer);
            _dispatcher.Listen("playerview_enable", OnPlayerEnable);
            _dispatcher.Listen("playerview_disable", OnPlayerDisable);
            _dispatcher.Listen("playerview_interactprop", OnPlayerInteractWithProp);
            _dispatcher.Listen("playerview_turn", OnPlayerTurned);
            _dispatcher.Listen("playerview_move", OnPlayerMoved);
        }

        private void OnSetPlayer(object[] args)
        {
            View.SetPlayer(_player.worldPos, _player.worldDir);
        }

        private void OnPlayerEnable(object[] args)
        {
            View.EnablePlayerInput();
        }

        private void OnPlayerDisable(object[] args)
        {
            View.DisablePlayerInput();
        }

        private void OnPlayerInteractWithProp(object[] args)
        {
            View.DisablePlayerInput();
            Map.Prop prop = (Map.Prop)args[0];
            Debug.Log("interacted with prop: " + prop.prefab.name);
            View.EnablePlayerInput();
        }

        private void OnPlayerMoved(object[] args)
        {
            Camera.main.transform.TweenPosition(_player.GetPosition(), 0.5f)
                .SetEasing(Easing.Sinusoidal.Out)
                .OnComplete.AddListener(View.EnablePlayerInput);
        }

        private void OnPlayerTurned(object[] args)
        {
            Camera.main.transform.TweenLocalRotation(Quaternion.LookRotation(_player.GetFacingDirection()), 0.5f)
                .SetEasing(Easing.Sinusoidal.Out)
                .OnComplete.AddListener(View.EnablePlayerInput);
        }

        private void OnPlayerAction(PlayerAction act)
        {
            View.DisablePlayerInput();
            _dispatcher.Dispatch("player_act", new object[] { act });
        }
    }

    public class PlayerView : View
    {
        public ISignal<PlayerAction> action = new Signal<PlayerAction>();
        public bool movementEnabled = true;
        public bool turningEnabled = true;

        public void SetPlayer(Vector2 pos, Vector2 facing)
        {
            Camera.main.transform.SetPositionAndRotation(new Vector3(pos.x, 0, pos.y), Quaternion.LookRotation(new Vector3(facing.x, 0, facing.y)));
        }

        public void EnablePlayerInput()
        {
            turningEnabled = true;
            movementEnabled = true;
        }

        public void DisablePlayerInput()
        {
            turningEnabled = false;
            movementEnabled = false;
        }

        public void Update()
        {
            if (movementEnabled)
            {
                if (Input.GetKey(KeyCode.W))
                    action.Dispatch(PlayerAction.MoveForward);
                else if (Input.GetKey(KeyCode.A))
                    action.Dispatch(PlayerAction.MoveLeft);
                else if (Input.GetKey(KeyCode.D))
                    action.Dispatch(PlayerAction.MoveRight);
                else if (Input.GetKey(KeyCode.S))
                    action.Dispatch(PlayerAction.MoveBack);
            }

            if (turningEnabled)
            {
                if (Input.GetKey(KeyCode.Q))
                    action.Dispatch(PlayerAction.TurnLeft);
                else if (Input.GetKey(KeyCode.E))
                    action.Dispatch(PlayerAction.TurnRight);
            }
        }
    }
}
