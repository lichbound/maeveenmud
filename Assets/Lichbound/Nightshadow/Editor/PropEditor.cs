﻿using Lichbound.Nightshadow.Behaviours;
using Lichbound.Nightshadow.Editor.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace Lichbound.Nightshadow.Editor
{
    [ExecuteInEditMode]
    [CustomEditor(typeof(PropController))]
    public class PropEditor : UnityEditor.Editor
    {
        private PropController _controller;
        protected Rect _outerRect;
        protected Rect _innerRect;

        private void OnEnable()
        {
            _controller = (PropController)target;
            _outerRect = new Rect(10, 10, 220, 550);
            _innerRect = new Rect(_outerRect.x + 3, _outerRect.y + 3, _outerRect.width - 6, _outerRect.height - 6);
        }

        private void OnSceneGUI()
        {
            Handles.BeginGUI();
            GUI.Box(_outerRect, "", GUI.skin.box);
            EditorGUI.DropShadowLabel(_innerRect, "hello");
            Handles.EndGUI();
        }
    }
}
