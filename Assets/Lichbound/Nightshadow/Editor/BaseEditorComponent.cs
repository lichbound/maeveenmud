﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor
{
    public abstract class BaseEditorComponent
    {
        public bool isActive = true;

        protected Rect _outerRect;
        protected Rect _innerRect;

        public BaseEditorComponent(int x, int y, int w, int h)
        {
            _outerRect = new Rect(x, y, w, h);
            _innerRect = new Rect(x + 3, y + 3, w - 6, h - 6);
        }

        public abstract void DrawGUI();
        public abstract void DrawHandles(Vector3 tilePos);
        public abstract void OnSceneMouseDown(Vector3 tilePos);
        public abstract void OnSceneMousePressed(Vector3 tilePos);

        public bool MouseInGUI()
        {
            return isActive && _outerRect.Contains(Event.current.mousePosition);
        }
    }
}
