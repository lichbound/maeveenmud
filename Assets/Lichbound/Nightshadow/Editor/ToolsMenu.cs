﻿using Lichbound.Nightshadow.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor.SceneManagement;
using UnityEditor;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

namespace Lichbound.Nightshadow.Editor
{
    public class ToolsMenu
    {
        [MenuItem("Nightshadow/Create Map")]
        public static void CreateNewMap()
        {
            if (!Directory.Exists(Application.dataPath + "/__DESIGN"))
                AssetDatabase.CreateFolder("Assets", "__DESIGN");
            if (!Directory.Exists(Application.dataPath + "/__DESIGN/Maps"))
                AssetDatabase.CreateFolder("Assets/__DESIGN", "Maps");

            string path = EditorUtility.SaveFilePanel("Save Map", Application.dataPath + "/__DESIGN/Maps", "NewMap", "unity");
            if (path != null)
            {
                Scene newMap = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
                EditorSceneManager.SaveScene(newMap, path);
                GameObject level = new GameObject("Level");
                GameObject props = new GameObject("Props");
                level.AddComponent<MapController>();
                AssetDatabase.SaveAssets();
                Selection.activeObject = level;
            }
        }
    }
}
