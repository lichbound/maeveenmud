﻿using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using static Lichbound.Nightshadow.Scriptables.Map;

namespace Lichbound.Nightshadow.Editor.Helpers
{
    public sealed class TileHelpers
    {
        private static Plane _ground = new Plane(Vector3.up, new Vector3(0, 0, 0));
        private static UnityEngine.Object _tilePrefab;
        private static Dictionary<Vector2Int, Map.WallType> _directions = new Dictionary<Vector2Int, Map.WallType>()
        {
            { Vector2Int.right, Map.WallType.West },
            { Vector2Int.left, Map.WallType.East },
            { Vector2Int.up, Map.WallType.North },
            { Vector2Int.down, Map.WallType.South }
        };

        public static Vector2Int GetTilePos2D(Vector3 tilePos)
        {
            return new Vector2Int(Mathf.FloorToInt(tilePos.x), Mathf.FloorToInt(tilePos.z));
        }

        public static Vector3 FindTileRectInScene()
        {
            Vector3 mousePos = Event.current.mousePosition;
            Ray guiRay = HandleUtility.GUIPointToWorldRay(mousePos);
            Vector3 hit = Vector3.zero;
            float dist = 0f;

            if (_ground.Raycast(guiRay, out dist))
                hit = guiRay.GetPoint(dist);

            if (hit == Vector3.zero)
                return Vector3.negativeInfinity;

            return new Vector3(Mathf.Round(hit.x), 0.1f, Mathf.Round(hit.z));
        }

        public static GameObject PlaceTile(GameObject container, Vector3 tilePos, bool[] activeWalls, WallTexture wt = null)
        {
            if (_tilePrefab == null)
                _tilePrefab = Resources.Load("Tile");

            GameObject newTile = (GameObject)PrefabUtility.InstantiatePrefab(_tilePrefab, container.transform);
            PrefabUtility.UnpackPrefabInstance(newTile, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
            newTile.name = GetTilePos2D(tilePos).ToString();
            newTile.transform.position = tilePos;
            TileController ctrl = newTile.GetComponent<TileController>();
            ctrl.SetPosition(GetTilePos2D(tilePos));
            ctrl.SetDefaultWallTextures(wt);
            ctrl.UpdateActiveWalls(activeWalls);
            ctrl.UpdateWallUVs(wt);
            return newTile;
        }

        public static GameObject PlaceContextAwareTile(GameObject container, Vector3 tilePos, bool[] activeWalls, WallTexture wt = null)
        {
            GameObject newTile = PlaceTile(container, tilePos, null, wt);
            UpdateTileWalls(container, newTile, tilePos, activeWalls);
            return newTile;
        }

        public static void UpdateTileWalls(GameObject container, GameObject tile, Vector3 tilePos, bool[] activeWalls = null, int depth = 0)
        {
            Vector2Int tilePos2d = GetTilePos2D(tilePos);
            TileController curCtrl = tile.GetComponent<TileController>();
            if (activeWalls == null)
                activeWalls = curCtrl.tile.activeWalls;

            foreach (var dir in _directions)
            {
                Vector2Int newPos2d = tilePos2d + dir.Key;
                Vector3 newPos = new Vector3(newPos2d.x, 0, newPos2d.y);
                Transform tileTransform = container.transform.Find(newPos2d.ToString());
                if (tileTransform != null)
                {
                    activeWalls[(int)dir.Value] = false;

                    if (depth == 0)
                        UpdateTileWalls(container, tileTransform.gameObject, newPos, null, 1);
                }
                else
                {
                    activeWalls[(int)dir.Value] = true;
                }
            }

            curCtrl.UpdateActiveWalls(activeWalls);
        }
    }
}
