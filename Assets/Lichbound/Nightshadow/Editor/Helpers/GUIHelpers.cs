﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor.Helpers
{
    public sealed class GUIHelpers
    {
        public static bool DrawCheckBox(string txt, float x, float y, bool val)
        {
            Rect rect = new Rect(x, y, 100, 20);
            return EditorGUI.Toggle(rect, txt, val);
        }

        public static bool DrawButton(string txt, float x, float y)
        {
            Rect rect = new Rect(x, y, 100, 24);
            return GUI.Button(rect, txt);
        }

        public static bool DrawButton(Texture2D img, float x, float y)
        {
            Rect rect = new Rect(x, y, img.width, img.height);
            return GUI.Button(rect, img);
        }

        public static void DrawLabel(string txt, float x, float y, bool boldText = false)
        {
            Rect rect = new Rect(x, y, 150, 25);
            GUIStyle style = boldText ? EditorStyles.boldLabel : null;
            GUI.Label(rect, txt, style);
        }
    }
}
