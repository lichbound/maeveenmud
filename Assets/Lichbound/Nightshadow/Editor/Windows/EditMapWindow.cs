﻿using Lichbound.Nightshadow.Behaviours;
using Lichbound.Nightshadow.Editor.Helpers;
using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor.Windows
{
    public class EditMapWindow : BaseEditorComponent
    {
        private MapController _controller;
        //private WallTexture _wallTex;

        public EditMapWindow(MapController ctrl, int x, int y, int w, int h) : base(x, y, w, h)
        {
            _controller = ctrl;
            //_wallTex = new WallTexture("Textures/Walls", "Textures/Walls");
        }

        override public void DrawGUI()
        {
            GUI.Box(_outerRect, "", GUI.skin.box);
            float y = _innerRect.y + 3;
            _controller.contextAware = GUIHelpers.DrawCheckBox("Context Aware", _innerRect.x + 3, y, _controller.contextAware);
            y += 25;

            for (int i = 0; i < _controller.drawActiveWalls.Length; i++)
            {
                if ((Map.WallType)i == Map.WallType.Ceiling || (Map.WallType)i == Map.WallType.Floor)
                    GUI.enabled = true;
                else if (_controller.contextAware)
                    GUI.enabled = false;
                _controller.drawActiveWalls[i] = GUIHelpers.DrawCheckBox(Enum.GetName(typeof(Map.WallType), i), _innerRect.x + 3, y, _controller.drawActiveWalls[i]);
                y += 25;
            }
            GUI.enabled = true;

            if (GUIHelpers.DrawButton(_controller.wallTexture == null ? "<None>" : _controller.wallTexture.name, _innerRect.x + 3, y))
            {
                EditorGUIUtility.ShowObjectPicker<Texture2D>(_controller.wallTexture, false, "", 0);
            }
        }

        public override void DrawHandles(Vector3 tilePos)
        {
            Handles.color = Color.white;
            Handles.DrawWireCube(tilePos, Vector3.one);

            if (Event.current.commandName.Equals("ObjectSelectorUpdated"))
            {
                _controller.wallTexture = (Texture2D)EditorGUIUtility.GetObjectPickerObject();
                string path = Path.ChangeExtension(AssetDatabase.GetAssetPath(_controller.wallTexture), "json");
                TextAsset wallJson = (TextAsset)AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset));
                if (wallJson != null)
                {
                    _controller.wallJson = wallJson;
                    _controller.CreateWallTexture();
                    //_controller.wt = new WallTexture(_controller.wallTexture, _controller.wallJson);
                    //_controller.wallIcons = _controller.wt.GetWallTextures();
                }
                else
                {
                    _controller.wt = null;
                    Debug.LogError("Could not find corresponding tile sheet json data for wall texture = " + path);
                }
            }
        }

        public override void OnSceneMouseDown(Vector3 tilePos)
        {
            Transform selectedTransform = Selection.activeGameObject.transform.Find(TileHelpers.GetTilePos2D(tilePos).ToString());
            if (selectedTransform == null && _controller.wt != null)
            {
                if (_controller.contextAware)
                    TileHelpers.PlaceContextAwareTile(Selection.activeGameObject, tilePos, _controller.drawActiveWalls, _controller.wt);
                else
                    TileHelpers.PlaceTile(Selection.activeGameObject, tilePos, _controller.drawActiveWalls, _controller.wt);
                EditorUtility.SetDirty(Selection.activeGameObject);
            }
        }

        public override void OnSceneMousePressed(Vector3 tilePos)
        {
        }
    }
}
