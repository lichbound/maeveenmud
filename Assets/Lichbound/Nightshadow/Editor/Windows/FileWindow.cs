﻿using Lichbound.Nightshadow.Behaviours;
using Lichbound.Nightshadow.Editor.Helpers;
using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor.Windows
{
    public class FileWindow : BaseEditorComponent
    {
        private MapController _controller;

        public FileWindow(MapController ctrl, int x, int y, int w, int h) : base(x, y, w, h)
        {
            _controller = ctrl;
        }

        public override void DrawGUI()
        {
            GUI.Box(_outerRect, "", GUI.skin.box);
            if (GUIHelpers.DrawButton("Save Map", _innerRect.x + 3, _innerRect.y + 3))
            {
                GenerateMap();
            }
        }

        public override void DrawHandles(Vector3 tilePos)
        {
        }

        public override void OnSceneMouseDown(Vector3 tilePos)
        {
        }

        public override void OnSceneMousePressed(Vector3 tilePos)
        {
        }

        private void GenerateMap()
        {
            Map map = ScriptableObject.CreateInstance<Map>();
            map.wallTexture = _controller.wallTexture;
            map.wallTextureData = _controller.wallJson;
            foreach (Transform transform in Selection.activeGameObject.transform)
            {
                TileController ctrl = transform.GetComponent<TileController>();
                map.tiles.Add(ctrl.tile);
            }

            GameObject props = GameObject.Find("Props");
            if (props != null)
            {
                foreach (Transform transform in props.transform)
                {
                    PropController ctrl = transform.GetComponent<PropController>();
                    ctrl.prop.pos = transform.position;
                    ctrl.prop.pos.y -= 0.1f;
                    ctrl.prop.prefab = PrefabUtility.GetCorrespondingObjectFromSource(transform.gameObject);
                    map.props.Add(ctrl.prop);
                }
            }
            else
                Debug.Log("No props found in map.");

            AssetDatabase.CreateAsset(map, "Assets/testmap.asset");
            AssetDatabase.SaveAssets();
        }
    }
}
