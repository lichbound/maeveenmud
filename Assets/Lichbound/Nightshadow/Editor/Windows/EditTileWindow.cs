﻿using Lichbound.Nightshadow.Behaviours;
using Lichbound.Nightshadow.Editor.Helpers;
using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor.Windows
{
    public class EditTileWindow : BaseEditorComponent
    {
        private MapController _controller;
        private List<GameObject> _selected = new List<GameObject>();
        private GameObject _hover;
        private WallSelector _selector;
        
        private int _selectForIndex = 0;
        private TileController _selectForCtrl = null;
        private bool _selectorOn = false;

        public EditTileWindow(MapController ctrl, int x, int y, int w, int h) : base(x, y, w, h)
        {
            _controller = ctrl;
            _selector = new WallSelector(x + w + 5, y, 70 * 3, h, 3, _controller.wt);
        }

        override public void DrawGUI()
        {
            GUI.Box(_outerRect, "", GUI.skin.box);
            if (_selected.Count == 1)
            {
                GUIHelpers.DrawLabel("Tile Properties", _innerRect.x + 3, _innerRect.y + 3, true);
                DrawTilePropertiesGUI();
            }
            else if (_selected.Count > 1)
            {
                GUIHelpers.DrawLabel("Edit Selected Tiles", _innerRect.x + 3, _innerRect.y + 3, true);
                DrawTileSelectionGUI();
            }

            if (_selectorOn)
            {
                int oldVal = _selector.selected;
                _selector.DrawGUI();
                if (oldVal != _selector.selected)
                {
                    _selectorOn = false;
                    _selectForCtrl.tile.wallTextures[_selectForIndex] = _selector.GetName(_selector.selected);
                    _selectForCtrl.UpdateWallUVs(_controller.wt);
                }
            }
        }

        private void DrawTilePropertiesGUI()
        {
            float y = _innerRect.y + 3 + 30;
            bool isActiveDirty = false;
            bool isTextureDirty = false;
            TileController ctrl = _selected[0].GetComponent<TileController>();

            for (int i = 0; i < ctrl.tile.activeWalls.Length; i++)
            {
                bool oldVal = ctrl.tile.activeWalls[i];
                ctrl.tile.activeWalls[i] = GUIHelpers.DrawCheckBox(Enum.GetName(typeof(Map.WallType), i), _innerRect.x + 3, y, ctrl.tile.activeWalls[i]);
                if (oldVal != ctrl.tile.activeWalls[i])
                    isActiveDirty = true;
                y += 25;

                string tex = ctrl.tile.wallTextures[i];
                if (GUIHelpers.DrawButton(_controller.wallIcons[tex], _innerRect.x + 3, y))
                {
                    _selector.selected = _selector.GetIndex(tex);
                    _selectorOn = !_selectorOn;
                    _selectForCtrl = ctrl;
                    _selectForIndex = i;
                }
                y += 68;
            }

            if (isActiveDirty)
                ctrl.UpdateActiveWalls(ctrl.tile.activeWalls);
            if (isTextureDirty)
                ctrl.UpdateWallUVs(_controller.wt);
        }

        private void DrawTileSelectionGUI()
        {

        }

        public override void DrawHandles(Vector3 tilePos)
        {
            if (_selected.Count > 0)
            {
                Handles.color = Color.red;
                foreach (var s in _selected)
                    Handles.DrawWireCube(s.transform.position, Vector3.one);
                
                /*Gizmos.color = Color.green;
                foreach (var s in _selected)
                    Gizmos.DrawWireCube(s.transform.position, Vector3.one);*/

                //Handles.DrawOutline(_selected, Color.cyan, 223);
                //Handles.color = Color.red;
                //Handles.DrawWireCube(_selected.position, Vector3.one);
            }

            Transform selectedTransform = Selection.activeGameObject.transform.Find(TileHelpers.GetTilePos2D(tilePos).ToString());
            if (selectedTransform != null)
            {
                Handles.color = Color.yellow;
                Handles.DrawWireCube(tilePos, Vector3.one);
                _hover = selectedTransform.gameObject;
            }
            else
                _hover = null;
        }

        public override void OnSceneMouseDown(Vector3 tilePos)
        {
        }

        public override void OnSceneMousePressed(Vector3 tilePos)
        {
            if (_hover != null)
            {
                if (!_selected.Contains(_hover))
                    _selected.Add(_hover);
                else
                    _selected.Remove(_hover);
            }
        }
    }
}
