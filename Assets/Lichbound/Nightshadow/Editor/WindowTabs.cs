﻿using Lichbound.Nightshadow.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor
{
    public class WindowTabs : BaseEditorComponent
    {
        private MapController _controller;
        private List<GUIContent> _options;

        public WindowTabs(MapController ctrl, int x, int y, int w, string[] options) : base(x, y, w, 30 * options.Length)
        {
            _controller = ctrl;
            _options = new List<GUIContent>();
            foreach (var option in options)
                _options.Add(new GUIContent(option));
        }

        public override void DrawGUI()
        {
            GUI.Box(_outerRect, "", GUI.skin.box);
            _controller.selectedTool = GUI.SelectionGrid(_innerRect, _controller.selectedTool, _options.ToArray(), 1);
        }

        public override void DrawHandles(Vector3 tilePos)
        {
        }

        public override void OnSceneMouseDown(Vector3 tilePos)
        {
        }

        public override void OnSceneMousePressed(Vector3 tilePos)
        {
        }
    }
}
