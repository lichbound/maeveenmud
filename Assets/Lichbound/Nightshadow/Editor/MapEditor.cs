﻿using Codice.Client.BaseCommands.BranchExplorer;
using Lichbound.Nightshadow.Behaviours;
using Lichbound.Nightshadow.Editor.Helpers;
using Lichbound.Nightshadow.Editor.Windows;
using Lichbound.Nightshadow.Scriptables;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor
{
    [ExecuteInEditMode]
    [CustomEditor(typeof(MapController))]
    public class MapEditor : UnityEditor.Editor
    {
        private MapController _controller;
        private Vector3 _currentTile;

        private bool _isMouseDown = false;
        private bool _isMousePressed = false;
        private Tool _rememberTool = Tool.Transform;
        private bool _mouseInGUI = false;

        private WindowTabs _windowTabs;
        private BaseEditorComponent[] _windows;
        private EditTileWindow _editTileWindow;
        private EditMapWindow _editMapWindow;
        private FileWindow _fileWindow;

        private void Awake()
        {

        }

        private void OnSceneGUI()
        {
            if (_controller != null && _windows != null)
            {
                if (Event.current.type == EventType.Repaint)
                    SceneView.RepaintAll();

                DoInput();
                DoDrawing();
                DisplayHandles();

                Handles.BeginGUI();
                _windowTabs.DrawGUI();
                _windows[_controller.selectedTool].DrawGUI();
                Handles.EndGUI();
            }
        }

        private void OnEnable()
        {
            _controller = (MapController)target;   // TODO --- toggle ceiling walls because they are usually in the way when editing.
            _controller.CreateWallTexture();

            _windowTabs = new WindowTabs(_controller, 10, 10, 170, new string[] { "Map", "Tile", "File" });
            _editMapWindow = new EditMapWindow(_controller, 10, 200, 200, 220);
            _editTileWindow = new EditTileWindow(_controller, 10, 200, 200, 620);
            _fileWindow = new FileWindow(_controller, 10, 200, 200, 220);
            _windows = new BaseEditorComponent[] { _editMapWindow, _editTileWindow, _fileWindow };

            _rememberTool = Tools.current;
            Tools.current = Tool.None;
        }

        private void OnDisable()
        {
            Tools.current = _rememberTool;
        }

        private void DoDrawing()
        {
            if (!_mouseInGUI)
            {
                if (_isMouseDown)
                    _windows[_controller.selectedTool].OnSceneMouseDown(_currentTile);
                else if (_isMousePressed)
                    _windows[_controller.selectedTool].OnSceneMousePressed(_currentTile);
            }
        }

        private void DoInput()
        {
            _mouseInGUI = _windowTabs.MouseInGUI() || _windows[_controller.selectedTool].MouseInGUI();

            if (Event.current.type == EventType.Layout)
                HandleUtility.AddDefaultControl(0);

            if (!_mouseInGUI)
            {
                _isMousePressed = false;
                if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
                    _isMouseDown = true;
                else if (_isMouseDown && Event.current.type == EventType.MouseUp)
                {
                    _isMousePressed = true;
                    _isMouseDown = false;
                }
            }
        }

        private void DisplayHandles()
        {
            Handles.color = Color.yellow;
            Handles.DrawSolidDisc(_controller.transform.position, Vector3.up, 0.1f);

            if (!_mouseInGUI)
            {
                Vector3 pos = TileHelpers.FindTileRectInScene();
                if (!pos.Equals(Vector3.negativeInfinity))
                    _currentTile = pos;

                _windows[_controller.selectedTool].DrawHandles(_currentTile);
            }
        }
    }
}
