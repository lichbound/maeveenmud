﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Editor
{
    public class WallSelector : BaseEditorComponent
    {
        public int selected = 0;

        private Rect _selectRect;
        private int _numPerRow;
        private Vector2 _scrollPos = Vector2.zero;
        private Dictionary<string, Texture2D> _wallTex = null;
        private GUIContent[] _content;

        public WallSelector(int x, int y, int w, int h, int numPerRow, WallTexture wt) : base(x, y, w, h)
        {
            _wallTex = wt.GetWallTextures();
            _numPerRow = numPerRow;
            CreateContent();

            int ww = _wallTex.First().Value.width + 5;
            int wh = _wallTex.First().Value.height + 5;
            _selectRect = new Rect(0, 0, ww * numPerRow, Mathf.CeilToInt(_wallTex.Count / numPerRow) * wh);
        }

        public override void DrawGUI()
        {
            GUI.Box(_outerRect, "", GUI.skin.box);
            _scrollPos = GUI.BeginScrollView(_innerRect, _scrollPos, _selectRect);
            selected = GUI.SelectionGrid(_selectRect, selected, _content, _numPerRow);
            GUI.EndScrollView();
        }

        public override void DrawHandles(Vector3 tilePos)
        {
        }

        public string GetSelected()
        {
            return _wallTex.Skip(selected).First().Key;
        }

        public int GetIndex(string key)
        {
            for (int i = 0; i < _content.Length; i++)
                if (_content[i].tooltip.Equals(key))
                    return i;
            return -1;
        }

        public string GetName(int index)
        {
            return _content[index].tooltip;
        }

        public override void OnSceneMouseDown(Vector3 tilePos)
        {
        }

        public override void OnSceneMousePressed(Vector3 tilePos)
        {
        }

        private void CreateContent()
        {
            _content = new GUIContent[_wallTex.Count];
            int idx = 0;
            foreach (var item in _wallTex)
            {
                GUIContent content = new GUIContent(item.Value, item.Key);
                _content[idx++] = content;
            }
        }
    }
}
