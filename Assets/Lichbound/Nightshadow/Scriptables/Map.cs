﻿using Lichbound.Nightshadow.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Scriptables
{
    [Serializable]
    public class Map : ScriptableObject
    {
        public Texture2D wallTexture;
        public TextAsset wallTextureData;
        public List<Tile> tiles = new List<Tile>();
        public List<Prop> props = new List<Prop>();

        [Serializable]
        public class Tile
        {
            public Vector2Int pos = Vector2Int.zero;
            public bool[] activeWalls = new bool[Enum.GetValues(typeof(WallType)).Length];
            public string[] wallTextures = new string[Enum.GetValues(typeof(WallType)).Length];
            private static Vector2Int[] _dirs = new Vector2Int[]
            {
                Vector2Int.up,
                Vector2Int.down,
                Vector2Int.right,
                Vector2Int.left
            };

            public void ForEach(Action<WallType, bool, string> cb)
            {
                for (int i = 0; i < activeWalls.Length; i++)
                {
                    cb((WallType)i, activeWalls[i], wallTextures[i]);
                }
            }

            public bool IsBlocked(Tile to)
            {
                Vector2Int d = to.pos - pos;
                for (int i = 0; i < _dirs.Length; i++)
                {
                    if (d.Equals(_dirs[i]))
                        return activeWalls[i];
                }
                return false;
            }
        }

        [Serializable]
        public class Prop
        {
            public Vector3 pos = Vector3.zero;
            public UnityEngine.Object prefab;
            public int interactRange = 0;
            public bool blocksPath = false;
            public List<IAction> actions = new List<IAction>();

            public Vector2Int GetMapPosition()
            {
                return new Vector2Int(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.z));
            }

            public bool WithinRange(Vector2Int target)
            {
                Vector2Int propPos = GetMapPosition();
                Vector2Int d = target - propPos;
                return d.magnitude <= interactRange;
            }
        }

        public interface IAction
        {
            void MonkeyBrain();
        }

        [Serializable]
        public enum WallType : int
        {
            North,
            South,
            West,
            East,
            Floor,
            Ceiling
        }
    }
}
