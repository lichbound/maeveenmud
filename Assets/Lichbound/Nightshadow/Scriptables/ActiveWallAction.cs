﻿using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Scriptables
{
    [Serializable]
    public class ActiveWallAction : Map.IAction
    {
        public Vector2Int pos;
        public Map.WallType wall;
        public bool isActive;

        public void MonkeyBrain()
        {
            Debug.Log("This is monkey brains!");
        }
    }
}
