using Lichbound.Nightshadow.Commands;
using Lichbound.Nightshadow.Models;
using Lichbound.Nightshadow.Views;
using Lichbound.WraithLib.Mvc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lichbound.Nightshadow
{
    public class NightshadowContext : Context
    {
        protected override void BindCommands()
        {
            BindCommand<StartStuff>();
            BindCommand<ExecutePlayerAction>();
        }

        protected override void BindMediators()
        {
            BindMediator<PlayerView, PlayerMediator>();
            BindMediator<MapView, MapMediator>();
        }

        protected override void BindModels()
        {
            BindModel<PlayerModel>();
            BindModel<MapModel>();
        }

        protected override void EnterContext()
        {
            MediateView<PlayerView>();
            MediateView<MapView>();

            _dispatcher.Dispatch("Start");
        }
    }
}
