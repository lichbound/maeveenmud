﻿using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Behaviours
{
    public class MapController : MonoBehaviour
    {
        public int selectedTool = 0;
        public bool contextAware = true;
        public bool[] drawActiveWalls = new bool[Enum.GetValues(typeof(Map.WallType)).Length];
        public Texture2D wallTexture;
        public TextAsset wallJson;
        public WallTexture wt;
        public Dictionary<string, Texture2D> wallIcons;

        public void CreateWallTexture()
        {
            if (wallTexture != null && wallJson != null)
            {
                wt = new WallTexture(wallTexture, wallJson);
                wallIcons = wt.GetWallTextures();
            }
        }
        //  TODO --- this should prob just contain settings and stuff that i want to persist.
        //public Map map;

        //private void Awake()
        //{
        //    map = ScriptableObject.CreateInstance<Map>();
        //}
    }
}
