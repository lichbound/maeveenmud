﻿using Lichbound.Nightshadow.Scriptables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow
{
    public class TileController : MonoBehaviour
    {
        public Map.Tile tile = new Map.Tile();

        public void SetPosition(Vector2Int pos)
        {
            tile.pos = pos;
        }

        public void SetDefaultWallTextures(WallTexture wt)
        {
            if (wt != null)
            {
                string[] wallNames = wt.GetWallNames();
                for (int i = 0; i < tile.wallTextures.Length; i++)
                    tile.wallTextures[i] = wallNames[0];
            }
        }

        public void UpdateWallUVs(WallTexture wt)
        {
            if (wt != null)
            {
                tile.ForEach((type, active, texture) =>
                {
                    GameObject w = GetWall(type);
                    MeshFilter mf = w.GetComponent<MeshFilter>();
                    Mesh m = Instantiate(mf.sharedMesh);
                    m.SetUVs(0, wt.GetUV(texture));
                    mf.sharedMesh = m;
                    tile.wallTextures[(int)type] = texture;
                });
            }
        }

        public void UpdateActiveWalls(bool[] activeWalls)
        {
            if (activeWalls != null)
            {
                tile.ForEach((type, active, texture) =>
                {
                    tile.activeWalls[(int)type] = activeWalls[(int)type];
                    if (activeWalls[(int)type])
                        GetWall(type).SetActive(true);
                    else
                        GetWall(type).SetActive(false);
                });
            }
        }

        public bool IsWallSet(Map.WallType wall)
        {
            Transform t = transform.Find(Enum.GetName(typeof(Map.WallType), wall));
            return t.gameObject.activeInHierarchy;
        }

        public GameObject GetWall(Map.WallType wall, bool opposite = false)
        {
            if (opposite)
            {
                if (wall == Map.WallType.North)
                    wall = Map.WallType.South;
                else if (wall == Map.WallType.South)
                    wall = Map.WallType.North;
                else if (wall == Map.WallType.West)
                    wall = Map.WallType.East;
                else if (wall == Map.WallType.East)
                    wall = Map.WallType.West;
            }

            Transform t = transform.Find(Enum.GetName(typeof(Map.WallType), wall));
            if (t != null)
                return t.gameObject;
            return null;
        }
    }
}
