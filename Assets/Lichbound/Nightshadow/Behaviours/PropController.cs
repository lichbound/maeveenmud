﻿using Lichbound.Nightshadow.Scriptables;
using Lichbound.WraithLib.Signals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Behaviours
{
    public class PropController : MonoBehaviour
    {
        public Map.Prop prop = new Map.Prop();
        public ISignal<Map.Prop> onClicked;

        private void OnMouseUpAsButton()
        {
            if (onClicked != null)
                onClicked.Dispatch(prop);
        }
    }
}
