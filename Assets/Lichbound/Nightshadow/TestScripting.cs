﻿using Lichbound.WraithLib.Scripting;
using Lichbound.WraithLib.Scripting.Symbols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Assets.Lichbound.Nightshadow
{
    public class TestScripting : MonoBehaviour
    {
        private void Awake()
        {
            QualiaVM vm = new QualiaVM();
            vm.Reset();
            vm.SetVariable("hunter", 14);
            vm.SetVariable("cake", false);
            vm.SetFunction("print", (args) => { Debug.Log(args[0]); return null; }, ValueKind.Void, "msg");

            //int asdf = 5;
            //asdf += 55;
            //Debug.Log(asdf);

            Lexer.SetDictionaries();
            //int hunter = 55;
            //int a = hunter++ - (4 + (1 * 2)) * 2;
            //Debug.Log(a);

            vm.Execute(@" a=5; a*=2-1; print(a);");
            //vm.Execute(" class Muppe { mongo = 4;  func construct(guf) { mongo = guf * guf; } func gaga(yt) { return mongo * yt; } }   a=new Muppe(4);  b=new Muppe(10);  print(\"gaga is \" + b.gaga(2)); ");
        }

        public void Testing(int aoa)
        {

        }
    }
}
