﻿using Lichbound.Nightshadow.Scriptables;
using Lichbound.WraithLib.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Models
{
    public class MapModel : IModel
    {
        private Map _map;

        public void LoadMap(string assetName)
        {
            _map = ScriptableObject.Instantiate<Map>(Resources.Load<Map>(assetName));
        }

        public bool IsPathBlocked(int x, int y, int x2, int y2)
        {
            Map.Tile from = GetTile(x, y);
            Map.Tile to = GetTile(x2, y2);
            bool blockingProps = IsPropsBlocking(GetProps(x2, y2));
            return blockingProps || from.IsBlocked(to) || to.IsBlocked(from);
        }

        public Map.Prop[] GetProps(int x, int y)
        {
            Vector2Int target = new Vector2Int(x, y);
            return _map.props.Where(p => target.Equals(p.GetMapPosition())).ToArray();
        }

        public bool IsPropsBlocking(Map.Prop[] props)
        {
            return props.Count(p => p.blocksPath) > 0;
        }

        public Map.Tile GetTile(int x, int y)
        {
            Vector2Int pos = new Vector2Int(x, y);
            return  _map.tiles.Where(t => t.pos.Equals(pos)).FirstOrDefault();
        }

        public void SetTile(int x, int y, Map.Tile tile)
        {
            Map.Tile t = GetTile(x, y);
            if (t != null)
                _map.tiles.Remove(t);
            _map.tiles.Add(tile);
        }

        public List<Map.Tile> GetTiles()
        {
            return _map.tiles;
        }

        public void ForEach(Action<Map.Tile> cb, Action<Map.Prop> cb2)
        {
            foreach (var t in _map.tiles)
                cb(t);

            foreach (var p in _map.props)
                cb2(p);
        }
    }
}
