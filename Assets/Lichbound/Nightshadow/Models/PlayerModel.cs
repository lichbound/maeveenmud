﻿using Lichbound.WraithLib.Extensions;
using Lichbound.WraithLib.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lichbound.Nightshadow.Models
{
    public class PlayerModel : IModel
    {
        public Vector2 worldPos = Vector2.zero;
        public Vector2 worldDir = Vector2.up;

        public Vector3 GetPosition()
        {
            return new Vector3(worldPos.x, 0, worldPos.y);
        }

        public Vector3 GetFacingDirection()
        {
            return new Vector3(worldDir.x, 0, worldDir.y);
        }
    }
}
