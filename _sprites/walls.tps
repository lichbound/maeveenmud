<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>5</int>
        <key>texturePackerVersion</key>
        <string>6.0.2</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Resources/Textures/Walls.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">walls/w3d_allwalls-0.png</key>
            <key type="filename">walls/w3d_allwalls-1.png</key>
            <key type="filename">walls/w3d_allwalls-10.png</key>
            <key type="filename">walls/w3d_allwalls-100.png</key>
            <key type="filename">walls/w3d_allwalls-101.png</key>
            <key type="filename">walls/w3d_allwalls-102.png</key>
            <key type="filename">walls/w3d_allwalls-103.png</key>
            <key type="filename">walls/w3d_allwalls-104.png</key>
            <key type="filename">walls/w3d_allwalls-105.png</key>
            <key type="filename">walls/w3d_allwalls-106.png</key>
            <key type="filename">walls/w3d_allwalls-107.png</key>
            <key type="filename">walls/w3d_allwalls-108.png</key>
            <key type="filename">walls/w3d_allwalls-109.png</key>
            <key type="filename">walls/w3d_allwalls-11.png</key>
            <key type="filename">walls/w3d_allwalls-12.png</key>
            <key type="filename">walls/w3d_allwalls-13.png</key>
            <key type="filename">walls/w3d_allwalls-14.png</key>
            <key type="filename">walls/w3d_allwalls-15.png</key>
            <key type="filename">walls/w3d_allwalls-16.png</key>
            <key type="filename">walls/w3d_allwalls-17.png</key>
            <key type="filename">walls/w3d_allwalls-18.png</key>
            <key type="filename">walls/w3d_allwalls-19.png</key>
            <key type="filename">walls/w3d_allwalls-2.png</key>
            <key type="filename">walls/w3d_allwalls-20.png</key>
            <key type="filename">walls/w3d_allwalls-21.png</key>
            <key type="filename">walls/w3d_allwalls-22.png</key>
            <key type="filename">walls/w3d_allwalls-23.png</key>
            <key type="filename">walls/w3d_allwalls-24.png</key>
            <key type="filename">walls/w3d_allwalls-25.png</key>
            <key type="filename">walls/w3d_allwalls-26.png</key>
            <key type="filename">walls/w3d_allwalls-27.png</key>
            <key type="filename">walls/w3d_allwalls-28.png</key>
            <key type="filename">walls/w3d_allwalls-29.png</key>
            <key type="filename">walls/w3d_allwalls-3.png</key>
            <key type="filename">walls/w3d_allwalls-30.png</key>
            <key type="filename">walls/w3d_allwalls-31.png</key>
            <key type="filename">walls/w3d_allwalls-32.png</key>
            <key type="filename">walls/w3d_allwalls-33.png</key>
            <key type="filename">walls/w3d_allwalls-34.png</key>
            <key type="filename">walls/w3d_allwalls-35.png</key>
            <key type="filename">walls/w3d_allwalls-36.png</key>
            <key type="filename">walls/w3d_allwalls-37.png</key>
            <key type="filename">walls/w3d_allwalls-38.png</key>
            <key type="filename">walls/w3d_allwalls-39.png</key>
            <key type="filename">walls/w3d_allwalls-4.png</key>
            <key type="filename">walls/w3d_allwalls-40.png</key>
            <key type="filename">walls/w3d_allwalls-41.png</key>
            <key type="filename">walls/w3d_allwalls-42.png</key>
            <key type="filename">walls/w3d_allwalls-43.png</key>
            <key type="filename">walls/w3d_allwalls-44.png</key>
            <key type="filename">walls/w3d_allwalls-45.png</key>
            <key type="filename">walls/w3d_allwalls-46.png</key>
            <key type="filename">walls/w3d_allwalls-47.png</key>
            <key type="filename">walls/w3d_allwalls-48.png</key>
            <key type="filename">walls/w3d_allwalls-49.png</key>
            <key type="filename">walls/w3d_allwalls-5.png</key>
            <key type="filename">walls/w3d_allwalls-50.png</key>
            <key type="filename">walls/w3d_allwalls-51.png</key>
            <key type="filename">walls/w3d_allwalls-52.png</key>
            <key type="filename">walls/w3d_allwalls-53.png</key>
            <key type="filename">walls/w3d_allwalls-54.png</key>
            <key type="filename">walls/w3d_allwalls-55.png</key>
            <key type="filename">walls/w3d_allwalls-56.png</key>
            <key type="filename">walls/w3d_allwalls-57.png</key>
            <key type="filename">walls/w3d_allwalls-58.png</key>
            <key type="filename">walls/w3d_allwalls-59.png</key>
            <key type="filename">walls/w3d_allwalls-6.png</key>
            <key type="filename">walls/w3d_allwalls-60.png</key>
            <key type="filename">walls/w3d_allwalls-61.png</key>
            <key type="filename">walls/w3d_allwalls-62.png</key>
            <key type="filename">walls/w3d_allwalls-63.png</key>
            <key type="filename">walls/w3d_allwalls-64.png</key>
            <key type="filename">walls/w3d_allwalls-65.png</key>
            <key type="filename">walls/w3d_allwalls-66.png</key>
            <key type="filename">walls/w3d_allwalls-67.png</key>
            <key type="filename">walls/w3d_allwalls-68.png</key>
            <key type="filename">walls/w3d_allwalls-69.png</key>
            <key type="filename">walls/w3d_allwalls-7.png</key>
            <key type="filename">walls/w3d_allwalls-70.png</key>
            <key type="filename">walls/w3d_allwalls-71.png</key>
            <key type="filename">walls/w3d_allwalls-72.png</key>
            <key type="filename">walls/w3d_allwalls-73.png</key>
            <key type="filename">walls/w3d_allwalls-74.png</key>
            <key type="filename">walls/w3d_allwalls-75.png</key>
            <key type="filename">walls/w3d_allwalls-76.png</key>
            <key type="filename">walls/w3d_allwalls-77.png</key>
            <key type="filename">walls/w3d_allwalls-78.png</key>
            <key type="filename">walls/w3d_allwalls-79.png</key>
            <key type="filename">walls/w3d_allwalls-8.png</key>
            <key type="filename">walls/w3d_allwalls-80.png</key>
            <key type="filename">walls/w3d_allwalls-81.png</key>
            <key type="filename">walls/w3d_allwalls-82.png</key>
            <key type="filename">walls/w3d_allwalls-83.png</key>
            <key type="filename">walls/w3d_allwalls-84.png</key>
            <key type="filename">walls/w3d_allwalls-85.png</key>
            <key type="filename">walls/w3d_allwalls-86.png</key>
            <key type="filename">walls/w3d_allwalls-87.png</key>
            <key type="filename">walls/w3d_allwalls-88.png</key>
            <key type="filename">walls/w3d_allwalls-89.png</key>
            <key type="filename">walls/w3d_allwalls-9.png</key>
            <key type="filename">walls/w3d_allwalls-90.png</key>
            <key type="filename">walls/w3d_allwalls-91.png</key>
            <key type="filename">walls/w3d_allwalls-92.png</key>
            <key type="filename">walls/w3d_allwalls-93.png</key>
            <key type="filename">walls/w3d_allwalls-94.png</key>
            <key type="filename">walls/w3d_allwalls-95.png</key>
            <key type="filename">walls/w3d_allwalls-96.png</key>
            <key type="filename">walls/w3d_allwalls-97.png</key>
            <key type="filename">walls/w3d_allwalls-98.png</key>
            <key type="filename">walls/w3d_allwalls-99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>walls</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
